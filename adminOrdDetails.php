<?php
session_start();
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
require("orderModel.php");
$ordID=(int)$_GET['ordID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HC's Shop</title>
</head>
<body>
<p>Customer's orders</p>
<a href="adminOrders.php">[Back]</a>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getOrderDetail($ordID);
?>
	<table width="600" border="1">
  <tr>
    <td>order ID</td>
	<td>uID</td>
    <td>Date</td>
    <td>Status</td>
	<td>Ordered product</td>
	<td>Price</td>
	<td>Quantity</td>
	<td>Amount</td>
  </tr>
<?php
$total=0;
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['ordID'] . "</td>";
	echo "<td>" , $rs['uID'] , "</td>";
	echo "<td>" , $rs['orderDate'] , "</td>";
	echo "<td>" , $rs['status'] , "</td>";
	echo "<td>" , $rs['name'] , "</td>";
	echo "<td>" , $rs['price'] , "</td>";
	echo "<td>" , $rs['quantity'] , "</td>";
	$total += $rs['quantity'] *$rs['price'];
	echo "<td>" , $rs['quantity'] *$rs['price'] , "</td>";
	//echo "<td><a href='setShipped.php?ordID=" , $rs['ordID'] ,"'>Done Packing!</a>";
	echo "</tr>";
}
echo "<tr><td>Total: $total</td></tr>";
?>
</table>
<a href="setShipped.php?ordID=<?php echo $ordID ?>">Done Packing!</a>
</body>
</html>
