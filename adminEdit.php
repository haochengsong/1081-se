<?php
session_start();
require("prdModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HC's Shop</title>
</head>
<body>
<p>Product Management UI </p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$prdID=(int)$_GET['prdID'];
	getPrdDetails($prdID);
	$result=getPrdDetails($prdID);
	$rs=NULL;
	
	if ($result) {
		$rs=mysqli_fetch_assoc($result);
	}
	if (!$rs) {
		$rs['prdID']=-1;
		$rs['name']='';
		$rs['price']=0;
		$rs['detail']='';
	}
?>
<form action="adminUpdate.php" method="POST">
<table width="200" border="1">
  <tr>
    <input type="hidden" name="prdID" value="<?php echo $rs['prdID'];?>"></tr>
    <tr><td>Name:<input type="text" name="name" value="<?php echo $rs['name'];?>"Required></td></tr>
    <tr><td>Price:<input type="text" name="price" value="<?php echo $rs['price'];?>"Required></td></tr>
    <tr><td>Detail:<input type="text" name="detail" value="<?php echo $rs['detail'];?>"Required></td></tr>
<tr><td><input type="submit"></td></tr>
</form>

</table>
<a href="adminMain.php">Back</a><hr>

</body>
</html>
