<?php
session_start();
//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

require("prdModel.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HC's Shop</title>
</head>
<body>
<?php
$prdID=$_POST['prdID'];
$prdDetails=array('prdID' => $prdID, 'name' => $_POST['name'], 'price' => (int)$_POST['price'], 'detail' => $_POST['detail']);

if ($prdID > 0) {
	prdUpdate($prdID, $prdDetails);
	echo "Product Updated!<br/>";
} else {
	addPrd($prdDetails);
	echo "Product Added!<br/>";
}
?>
<a href="adminMain.php">Back to main page</a>

</body>
</html>