<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HC's Shop</title>
</head>
<body>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getCartDetail($_SESSION["loginProfile"]["uID"]);
?>
<p>My Shopping Cart Details</p>
	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>Prd Name</td>
    <td>price</td>
    <td>Quantity</td>
    <td>Amount</td>
  </tr>
<?php
$total=0;
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['serno'] . "</td>";
	echo "<td>{$rs['name']}</td>";
	echo "<td>" , $rs['price'], "</td>";
	echo "<td>" , $rs['quantity'], "</td>";
	$total += $rs['quantity'] *$rs['price'];
	echo "<td>" , $rs['quantity'] *$rs['price'] , "</td>";
}
echo "<tr><td>Total: $total</td></tr>";
if ($total == 0) {
	echo "Error: No item in your Cart!";
	echo "<a href='main.php'> Go Shopping</a>";
	return false;
}
?>
</table>
<hr>
<form method="post" action="checkoutControl.php">
<p>Enter Your Shipping Address here!</p><br/>
<input type="text" name="address" Required><br/>
<input type='submit' name='submit' value='Submit'><br/>
<a href="showCartDetail.php">Back</a>
</form>
</body>
</html>
