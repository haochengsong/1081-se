<?php
session_start();
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
require("orderModel.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HC's Shop</title>
</head>
<body>
<p>Customer's orders</p>
<a href="adminMain.php">[Back]</a>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
?>
	<table width="400" border="1">
  <tr>
    <td>Order ID</td>
    <td>uID</td>
    <td>Order Date</td>
	<td>Status</td>
  </tr>
<?php
$result=getUserOrder();
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['ordID'] . "</td>";
	echo "<td>" , $rs['uID'] , "</td>";
	echo "<td>" , $rs['orderDate'] , "</td>";
	echo "<td>" , $rs['status'] , "</td>";
	echo "<td><a href='adminOrdDetails.php?ordID=" , $rs['ordID'] ,"'>Show Details</a></td>";
	echo "</tr>";
}
?>
</table>

</body>
</html>
