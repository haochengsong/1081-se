<?php
session_start();
require("prdModel.php");
require("userModel.php");

if ( ! isSet($_SESSION["loginProfile"] )) {
	header("Location: loginUI.php");
}
checkIdentity($_SESSION["loginProfile"]["uRole"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HC's Shop</title>
</head>
<body>
<p>Admin's Management page 
[<a href="logout.php">logout</a>]

</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getPrdList();
?>
<a href="adminOrders.php">Show Customer's Orders</a><br/>
<a href="adminEdit.php?prdID=-1">Add New Product</a><hr>
	<table width="600" border="1">
  <tr>
    <td>Id</td>
    <td>Product Name</td>
    <td>Price</td>
	<td>Details</td>
    <td>Edit</td>
	<td>Delete</td>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['prdID'] . "</td>";
	echo "<td>{$rs['name']}</td>";
	echo "<td>" , $rs['price'], "</td>";
	echo "<td>" , $rs['detail'] , "</td>";
	echo "<td><a href='adminEdit.php?prdID=" , $rs['prdID'] , "'>Edit</a></td>";
	echo "<td><a href='adminDelete.php?prdID=" , $rs['prdID'] , "'>Delete</a></td>";
}
?>
</table>

</body>
</html>
