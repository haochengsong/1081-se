<?php
require_once("dbconfig.php");

function getOrderList($uID) { //user function
	global $db;
	$sql = "SELECT userorder.ordID, userorder.orderDate, userorder.status, product.name, orderitem.quantity FROM userOrder, product, orderitem WHERE userOrder.ordID=orderitem.ordID and orderitem.prdID=product.prdID and userOrder.uID=? order by ordID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "insert into userOrder ( uID, status ) values (?,0)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function addToCart($uID, $prdID, $quantity) {
	global $db;
	$ordID= _getCartID($uID);
	$sql2 = "select ordID, prdID from orderItem where ordID=$ordID and prdID=$prdID";
	$result=mysqli_query($db,$sql2);
	// mysqli_query執行mysqli_prepare($db, $sql)和mysqli_stmt_execute
	if (mysqli_num_rows($result) == 1) { //執行成功或失敗 true and false
		$sql3 = "update orderItem set quantity = quantity + $quantity where ordID=$ordID and prdID=$prdID";
	    mysqli_query($db,$sql3);  //執行SQL
	} else {
		$sql = "insert into orderItem (ordID, prdID, quantity) values (?,?,$quantity);";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID);
	    mysqli_stmt_execute($stmt);  //執行SQL
	}
}

function removeFromCart($uID, $serno) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "delete from orderItem where serno=$serno";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_execute($stmt);
}

function checkout($uID, $address) {
	global $db;
	$ordID= _getCartID($uID);
    $sql = "update userOrder set orderDate=now(), address='$address', status='1' where ordID='$ordID'";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_execute($stmt);
    echo "Order Proceed!";
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getUserOrder() { //admin function
	global $db;
	$sql="select userorder.ordID, userorder.uID, userorder.orderDate, userorder.address, userorder.status, product.name, orderitem.quantity FROM userOrder, product, orderitem WHERE userOrder.ordID=orderitem.ordID and orderitem.prdID=product.prdID and status='1' order by ordID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getOrderDetail($ordID) {
	global $db;
	$sql="select userorder.ordID, userorder.uID, userorder.orderDate, userorder.address, userorder.status, product.name, product.price, orderitem.quantity FROM userOrder, product, orderitem WHERE userOrder.ordID=orderitem.ordID and orderitem.prdID=product.prdID and status='1' and orderitem.ordID=$ordID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function setShipped($ordID) {
	global $db;
	$sql="update userOrder set status='2' where ordID='$ordID'";
	mysqli_query($db,$sql);
}
?>










