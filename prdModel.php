<?php
require_once("dbconfig.php");

function getPrdList() {
	global $db;
	$sql = "SELECT prdID,name, price, detail FROM product order by prdID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getPrdDetails($prdID) {
	global $db;
	$sql = "SELECT prdID,name, price, detail FROM product where prdID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function prdUpdate($prdID, $prdDetails) {
	global $db;
	$sql = "update product set name=?, price=?, detail=? where prdID=$prdID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "sis", $prdDetails['name'], $prdDetails['price'], $prdDetails['detail']); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function addPrd($prdDetails) {
	global $db;
	$sql = "insert into product (name, price, detail) values (?,?,?)";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "sis", $prdDetails['name'], $prdDetails['price'], $prdDetails['detail']); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function deletePrd($prdID) {
	global $db;
	$sql = "delete from product where prdID=$prdID";
	mysqli_query($db,$sql);
}
?>










