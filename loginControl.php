<?php
session_start();

require("userModel.php");
$ID=$_POST['ID'];
$pwd=$_POST['PWD'];

$userProfile = getUserProfile( $ID, $pwd);
if ($userProfile) {
	//echo "Login Success! <br>";
	$_SESSION['loginProfile'] = $userProfile;
	if ($_SESSION['loginProfile']['uRole'] == 9) {
		header("Location: adminMain.php");
	} else {
	    header("Location: main.php");
	}
} else {
	echo "login failed <br>";
	$_SESSION['loginProfile'] = NULL;
	echo "<a href='loginUI.php'>Login again</a>";
}
?>
